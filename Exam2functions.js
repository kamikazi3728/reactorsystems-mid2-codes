

//Exam2functions.js

const colors = require('colors');

//function to start new part to the question
function startNewPart(n){
	console.log("============================================================================================");
	console.log("========================================== PART "+n+" ==========================================");
	console.log();
}

function blank(){console.log();}//function to display blank line

//Function to display code information
function displayInfo(title,author,dayString,part){console.log(title);console.log("Code written by: "+author);console.log("Code completed on: "+dayString);blank();startNewPart(part);}

let starLine = "******************************"; //A line of stars :)

//function for solution header
function solnHeader(prbn,prt){colorOut([starLine," Solution for Problem "+prbn+", Part "+prt+" ",starLine],[cyan,white,cyan]);blank();}

//function to write a string in a color
function colorme(value,color){return eval("(value.toString())."+color); }

//function to write a string with varying colors throughout specified values
function colorOut(strs,clrs){let sout = "";for(var i=0;i<strs.length;i++){sout=sout+colorme(strs[i],clrs[i]);}console.log(sout);}

//function that returns a string of 37 spaces
function spaces(){let nsp = 37;let spo = "";for(var i=0;i<nsp;i++){spo=spo+" ";}return spo;}

//Defining color constants as strings, because quotation marks
const green = "green";const cyan = "cyan";const yellow = "yellow";const magenta = "magenta";const white = "white";

//Set up exports
exports.snp = startNewPart;
exports.blk = blank;
exports.dI = displayInfo;
exports.sL = "******************************";
exports.sH = solnHeader;
exports.cme = colorme;
exports.c = {g:"green",c:"cyan",y:"yellow",m:"magenta",w:"white"};//Defining color constants as strings, because quotation marks
exports.cO = colorOut;
exports.sp = spaces;